package com.example.sysn.multipeerconnection.money

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase

import java.util.ArrayList
import java.util.EventListener

/**
 * Created by shiny on 2017/10/05.
 */

class GetDataSnapshots(interfase: listener, address: String) {
     var database = FirebaseDatabase.getInstance()
     var dataSnapshots = ArrayList<DataSnapshot>()

    init {
        database.getReference(address).addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                if (!dataSnapshots.contains(dataSnapshot!!)) {
                    dataSnapshots.add(dataSnapshot)
                    interfase.onDataSnapshot(dataSnapshots)
                }
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
                val value = dataSnapshot!!.key.split(",")[1]
                for ((i, d) in dataSnapshots.withIndex()) {
                    if (value == d.key.split(",")[1]) {
                        dataSnapshots.removeAt(i)
                        break
                    }
                }
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onCancelled(databaseError: DatabaseError?) {}
        })
    }

    interface listener : EventListener {
        fun onDataSnapshot(_dataSnapshots: ArrayList<DataSnapshot>)
    }
}