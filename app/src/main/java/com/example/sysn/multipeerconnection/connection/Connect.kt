package com.example.sysn.multipeerconnection.connection

import android.os.AsyncTask
import android.os.Handler
import android.os.HandlerThread
import com.example.sysn.multipeerconnection.etc.A.*
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import org.apache.commons.lang3.RandomStringUtils
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.etc.F.*
import shinyoshiaki.blockchain.webrtc.WebRTC
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by shiny on 2017/11/01.
 */

class Connect : WebRTC.Listener {
    var listener: Connect.Listener? = null
    val listenerRtc: WebRTC.Listener = this

    val RTCMAX = 3

    val database = FirebaseDatabase.getInstance()
    var offers = ArrayList<DataSnapshot>()
    val peerLabels = ArrayList<String>()

    inner class Peer(var rtc: WebRTC) {
        var label = ""

        init {
            while (true) {
                label = RandomStringUtils.randomAlphabetic(10)
                if (!peerLabels.contains(label)) {
                    peerLabels.add(label)
                    break
                }
            }
        }

        var isConnected = false
        var targetLabel: String? = null

        fun targetExist(): Boolean = rtc.targetExist()
        fun target(): String? = rtc.target
    }

    var peerO: Peer
    var peerA: Peer

    inner class Peers {
        var answer = arrayOfNulls<Peer>(RTCMAX)
        var offer = arrayOfNulls<Peer>(RTCMAX)

        private fun isNotFulled(rtcs: Array<Peer?>): Boolean = rtcs.any { !it!!.targetExist() }     //エニーはいずれか一つでも含まれていること

        fun answerIsNotFulled(): Boolean = isNotFulled(answer)
        fun offerIsNotFulled(): Boolean = isNotFulled(offer)

        private fun setPeer(peer: Peer, rtcs: Array<Peer?>) {
            for ((i, rtc) in rtcs.withIndex())
                if (!rtc!!.targetExist()) {
                    rtcs[i] = peer
                    break
                }
        }

        fun setPeerA(peer: Peer) = setPeer(peer, answer)
        fun setPeerO(peer: Peer) = setPeer(peer, offer)

        fun offerPeer() {
            for (rtc in offer) {
                if (!rtc!!.targetExist()) {
                    rtc.rtc.initOffer(rtc.label, MULTI)
                    break
                }
            }
        }

        fun isConnected(target: String?): Boolean {
            offer.filter { it!!.targetExist() && it.target() == target }
                    .forEach { return true }
            answer.filter { it!!.targetExist() && it.target() == target }
                    .forEach { return true }

            return false
        }

        fun connectedNum(): Int {
            var ans = 0
            fun job(rtcs: Array<Peer?>) {
                rtcs.filter { it!!.targetExist() }
                        .forEach { ans++ }
            }
            job(peers.answer)
            job(peers.offer)
            return ans
        }
    }

    val peers = Peers()

    class Devices {
        class Device(val id: String, val time: String)

        val v = ArrayList<Device>()

        fun add(id: String, time: String) {
            if (v.find { it.id == id } == null) {
                log("device added: $id")
                v.add(Device(id, time))
            }
        }

        fun remove(target: String) {
            for ((i, device) in v.withIndex()) {
                if (device.id == target) {
                    v.removeAt(i)
                    break
                }
            }
        }
    }

    val devices = Devices()

    fun isOfferExist(): Boolean {
        val clone = offers.clone() as ArrayList<DataSnapshot>
        return clone
                .map { it.key.split(",")[0] }
                .any { it == ANDROID }      //エニーはいずれか一つでも含まれていること
    }

    fun getPeer(_label: String): Peer? {
        peers.answer
                .filter { it!!.label == _label }
                .forEach { return it }
        peers.offer
                .filter { it!!.label == _label }
                .forEach { return it }

        return null
    }

    var isFindingPeer = false

    init {
        peerO = Peer(WebRTC(this))
        peerA = Peer(WebRTC(this))
        for (i in 0 until RTCMAX) peers.offer[i] = Peer(WebRTC(this))
        for (i in 0 until RTCMAX) peers.answer[i] = Peer(WebRTC(this))

        database.getReference("offer").addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                dataSnapshot?.let {
                    if (!offers.contains(it)) offers.add(it)
                }
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
                dataSnapshot?.let {
                    val target = it.key.split(",")[0]
                    for ((i, d) in offers.withIndex()) {
                        val name = d.key.split(",")[0]
                        if (target == name) {
                            offers.removeAt(i)
                            break
                        }
                    }
                }
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onCancelled(databaseError: DatabaseError?) {}
        })
        database.getReference("answer").addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                dataSnapshot?.let {
                    try {
                        val json = JSONObject(it.getValue(String::class.java))
                        val tar = json.getString("target")
                        val sdp = json.getString("sdp")
                        val from = json.getString("from")
                        val targetLabel = json.getString("label")
                        if (tar == ANDROID) {
                            if (!peerO.targetExist() && peerO.rtc.peerConnectionExist()) {
                                peerO.rtc.receiveOffer(sdp, from)
                                peerO.targetLabel = targetLabel
                                peerO.isConnected = true

                                Handler().postDelayed({ connectMulti() }, (1000).toLong())
                            }
                            it.ref.removeValue()
                        }
                    } catch (ignored: JSONException) {
                    }
                }
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {}

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {}

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        Handler().postDelayed({ peerO.rtc.initOffer(peerO.label, FIRST) }, (5 * 1000).toLong())

        Handler().postDelayed({
            val handlerLive = Handler()
            handlerLive.post(object : Runnable {
                override fun run() {
                    if (!isFindingPeer && peers.connectedNum() == 0) {
                        isFindingPeer = true

                        log("peers unconnected")

                        findFirstPeer()

                        if (!isOfferExist()) {
                            log("remade initOffer")
                            peerO.rtc = WebRTC(listenerRtc)
                            peerO.rtc.initOffer(peerO.label, FINAL)
                        }

                        Handler().postDelayed({ isFindingPeer = false }, (30 * 1000).toLong())
                    }
                    handlerLive.postDelayed(this, (1000).toLong())
                }
            })

            val handlerCheck = Handler()
            handlerCheck.post(object : Runnable {
                override fun run() {
                    if (peers.connectedNum() > 0) {
                        log("peers check survive")
                        checkSurvivePeers()
                    }
                    handlerCheck.postDelayed(this, 120000)
                }
            })
        }, (60 * 1000).toLong())
    }

    val PEERSO = "peersO"
    val PEERSA = "peersA"

    fun checkSurvivePeers() {
        fun job(peers: Array<Peer?>, type: String) {
            for ((i, peer) in peers.withIndex()) {
                Handler().postDelayed({
                    peer?.let {
                        if (it.targetExist()) {
                            log("$type($i) check")

                            sendPing(type, it.label)

                            Handler().postDelayed({
                                if (!it.isConnected) {
                                    if (it.targetExist()) {
                                        log("$type($i): " + it.target() + " is disconnected")

                                        devices.remove(it.rtc.target)
                                    }
                                    it.rtc = WebRTC(this)

                                    if (type == "peersO" && !isOfferExist()) {
                                        peerO.rtc = WebRTC(this)
                                        peerO.rtc.initOffer(FINAL)
                                    }
                                }
                            }, (20 * 1000).toLong())
                        }
                    }
                }, (i * 5 * 1000).toLong())
            }
        }

        job(peers.offer, "peersO")
        Handler().postDelayed({ job(peers.answer, "peersA") }, ((RTCMAX * 10 + 10) * 1000).toLong())
    }

    fun findFirstPeer() {
        log("findfirstpeer")
        peerA = Peer(WebRTC(this))

        val shots = offers.clone() as ArrayList<DataSnapshot>
        Collections.shuffle(shots)

        if (shots.size == 0) {
            log("find first peer 0")
            peerO = Peer(WebRTC(this))
            peerO.rtc.initOffer(peerO.label, FINAL)
        } else {
            for (dataSnapshot in shots) {
                val target = dataSnapshot.key.split(",")[0]
                val targetLabel = dataSnapshot.key.split(",")[1]
                val targetSdp = dataSnapshot.getValue(String::class.java)

                if (target != ANDROID) {
                    if (peerO.targetExist() && peerO.isConnected && target == peerO.rtc.target) {
                        log("skipped")
                        continue
                    }
                    log("try to " + target)
                    dataSnapshot.ref.removeValue()
                    peerA.rtc.initAnswer(peerA.label, FIRST)
                    peerA.rtc.receiveAnswer(targetSdp, target)
                    peerA.targetLabel = targetLabel
                    break
                }
            }
        }
    }

    var multiConnecting = false
    override fun onCommandChanged(command: String) {
        fun isCommandExist(command: String): Boolean {
            return try {
                JSONObject(command).let {
                    if (!msgId.list.contains(it.getString(MSGID))) {
                        msgId.list.add(it.getString(MSGID))
                        false
                    } else
                        true
                }
            } catch (e: JSONException) {
                true
            }
        }

        if (isCommandExist(command)) return

        listener?.onConnectCommand(command)

        bloadCast(command)

        try {
            JSONObject(command).let {

                devices.add(it.getString(AID), it.getString("TIME"))

                when (it.getString(ORDER)) {
                    "ping" -> pingReceived(JSONObject(command))
                    "pong" -> pongReceived(JSONObject(command))
                }

                connectMultiReceived(it)
            }
        } catch (ignored: JSONException) {
            log("connect onCommand parse error")
        }

        connectMulti()
    }

    fun connectMulti() {
        if (multiConnecting) return
        else multiConnecting = true

        if (peerA.targetExist() && peers.answerIsNotFulled()) {
            peers.setPeerA(peerA)
            peerA = Peer(WebRTC(this))
        }
        if (peerO.targetExist() && peers.offerIsNotFulled()) {
            peers.setPeerO(peerO)
            peerO = Peer(WebRTC(this))

            if (!isOfferExist()) {
                peerO.rtc.initOffer(peerO.label, FINAL)
                log("offer final")
            }
        }

        if (peers.offerIsNotFulled()) {
            peers.offerPeer()
        }

        val i = HandlerThread("i")
        i.start()
        Handler(i.looper).postDelayed({
            multiConnecting = false
        }, 10 * 1000)
    }

    fun connectMultiReceived(json: JSONObject) {
        try {
            val order = json.getString(ORDER)
            if (order.contains("signaling") && json.getString("target") == ANDROID) {
                val sdp = json.getString("sdp")
                val from = json.getString("from")

                when (order) {
                    "signaling_offer" -> {
                        log("multi get offer " + from)
                        for (peer in peers.answer) {
                            if (!peer!!.targetExist()) {
                                val targetLabel = json.getString("label")
                                peer.rtc.initAnswer(peer.label, MULTI, targetLabel)
                                peer.rtc.receiveAnswer(sdp, from)
                                peer.targetLabel = targetLabel
                                break
                            }
                        }
                    }
                    "signaling_answer" -> {
                        log("multi get answer " + from)
                        val targetLabel = json.getString("label")
                        val peer = getPeer(json.getString("offer_label"))
                        if (peer!!.rtc.peerConnectionExist()) {
                            peer.rtc.receiveOffer(sdp, from)
                            peer.targetLabel = targetLabel
                        }
                    }
                }
            }
        } catch (ignored: JSONException) {
        }
    }

    override fun onSdpAnswer(sdp: String?, args: Array<out String>) {
        if (args.size < 2) return

        val label = args[0]
        val work = args[1]

        when (work) {
            FIRST -> if (peerA.rtc.target != null) {
                val myRef = database.getReference("answer/" + ANDROID + "," + date2string(TIME_LOGIN))
                myRef.push()
                myRef.setValue(JSONObject()
                        .put("target", peerA.rtc.target)
                        .put("from", ANDROID)
                        .put("sdp", sdp)
                        .put("label", label)
                        .toString())

                log("firebase answer")

                val i = HandlerThread("i")
                i.start()
                Handler(i.looper).postDelayed({
                    if (!peerA.isConnected && peerA.targetExist()) {
                        sendPing("peerA")

                        val j = HandlerThread("j")
                        j.start()
                        Handler(j.looper).postDelayed({
                            if (!peerA.isConnected) {
                                log("first peerA failed")
                                findFirstPeer()
                            } else {
                                connectMulti()
                            }
                            myRef.ref.removeValue()
                        }, 20 * 1000)
                    }
                }, 20 * 1000)
            }
            MULTI -> {
                val peer = getPeer(label)
                log("from $ANDROID send answer to " + peer!!.rtc.target)
                shareJson(JSONObject()
                        .put("from", ANDROID)
                        .put("target", peer.rtc.target)
                        .put("offer_label", args[2])
                        .put("sdp", sdp)
                        .put("label", label)
                        .put(ORDER, "signaling_answer"))
            }
        }
    }

    override fun onSdpOffer(sdp: String?, args: Array<out String>) {
        if (args.size < 2) return

        val label = args[0]
        val work = args[1]

        fun sendOffer() {
            val clone = offers.clone() as ArrayList<DataSnapshot>
            for (dataSnapshot in clone) {
                val id = dataSnapshot.key.split(",")[0]
                if (id == ANDROID)
                    dataSnapshot.ref.removeValue()
            }

            val myRef = database.getReference("offer/$ANDROID,$label")
            myRef.push()
            myRef.setValue(sdp)

            log("firebase offer")
        }

        when (work) {
            FIRST -> {
                if (offers.size > 0)
                    findFirstPeer()

                sendOffer()
                log("first offerd")
            }
            MULTI -> {
                var aid = ""
                Collections.shuffle(devices.v)
                for (value in devices.v) {
                    if (!peers.isConnected(value.id)) {
                        aid = value.id
                        break
                    }
                }
                if (aid != "") {
                    log("connectMulti")
                    log("from $ANDROID send offer to $aid")

                    shareJson(JSONObject()
                            .put(ORDER, "signaling_offer")
                            .put("from", ANDROID)
                            .put("target", aid)
                            .put("label", label)
                            .put("sdp", sdp))
                }
            }
            FINAL -> {
                sendOffer()
                log("final offerd")
            }
        }
    }

    inner class BloadCast(var string: String) : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void): String? {
            fun job(rtcs: Array<Peer?>) {
                for (peer in rtcs) {
                    peer?.let {
                        if (it.targetExist()) it.rtc.sendThroughDataChannel(string)
                    }
                }
            }
            job(peers.offer)
            job(peers.answer)
            return null
        }
    }

    fun bloadCast(_command: String) {
        BloadCast(_command).execute()
    }


    inner class SendPing(val type: String, val label: String) : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void): String? {
            val send = JSONObject()
                    .put(AID, ANDROID)
                    .put(MSGID, msgId.get())
                    .put("TIME", date2string(TIME_LOGIN))
                    .put(ORDER, "ping")
                    .put("type", type)

            when (type) {
                "peerA" -> {
                    peerA.isConnected = false
                    peerA.rtc.sendThroughDataChannel(send.toString())
                }
                "peerO" -> {
                    peerO.isConnected = false
                    peerO.rtc.sendThroughDataChannel(send.toString())
                }
                else -> {
                    val peer = getPeer(label)
                    peer!!.isConnected = false
                    send.put("label", peer.label)
                    send.put("target_label", peer.targetLabel)
                    peer.rtc.sendThroughDataChannel(send.toString())
                }
            }
            return null
        }
    }

    fun sendPing(_type: String, vararg label: String) {
        if (label.isNotEmpty())
            SendPing(_type, label[0]).execute()
        else
            SendPing(_type, "").execute()
    }

    fun pingReceived(json: JSONObject) {
        try {
            val type = json.getString("type")
            val from = json.getString(AID)

            val send = JSONObject()
                    .put(AID, ANDROID)
                    .put(MSGID, msgId.get())
                    .put("TIME", date2string(TIME_LOGIN))
                    .put(ORDER, "pong")
                    .put("type", type)

            when (type) {
                "peerA" -> {
                    log("peerA ping from " + from)

                    peerO.isConnected = true
                    peerO.rtc.sendThroughDataChannel(send.toString())
                }
                "peerO" -> {
                    log("peerO ping from " + from)

                    peerA.isConnected = true
                    peerA.rtc.sendThroughDataChannel(send.toString())
                }
                else -> {
                    log("peers ping from " + from)

                    val label = json.getString("target_label")
                    val targetLabel = json.getString("label")
                    getPeer(label)?.let {
                        it.isConnected = true
                        send.put("label", label)
                        send.put("target_label", targetLabel)
                        it.rtc.sendThroughDataChannel(send.toString())
                    }
                }
            }
        } catch (ignored: JSONException) {
            log("ping received parse error")
        }
    }

    fun pongReceived(json: JSONObject) {
        try {
            val type = json.getString("type")
            val from = json.getString(AID)

            when (type) {
                "peerA" -> {
                    log("peerA pong from " + from)

                    peerA.isConnected = true
                }
                "peerO" -> {
                    log("peerO pong from " + from)

                    peerO.isConnected = true
                }
                else -> {
                    log("peers pong from " + from)

                    val label = json.getString("target_label")
                    getPeer(label)?.let {
                        it.isConnected = true
                    }
                }
            }
        } catch (ignored: JSONException) {
            log("pong received parse error")
        }
    }

    inner class ShareJson(var json: JSONObject) : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void): String? {
            try {
                bloadCast(JSONObject(json.toString())
                        .put(AID, ANDROID)
                        .put(MSGID, msgId.get())
                        .put("TIME", date2string(TIME_LOGIN))
                        .toString())
            } catch (ignored: JSONException) {
            }
            return null
        }
    }

    fun shareJson(json: JSONObject): String {
        ShareJson(json).execute()
        return json.toString()
    }

    interface Listener : EventListener {
        fun onConnectCommand(value: String)
    }
}