package com.example.sysn.multipeerconnection.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.etc.A.ANDROID
import com.example.sysn.multipeerconnection.etc.A.COIN_VALUE
import com.example.sysn.multipeerconnection.money.GetDataSnapshots
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_admin.*
import shinyoshiaki.blockchain.etc.F.toast
import java.util.*

/**
 * Created by shiny on 2017/09/20.
 */

class AdminActivity : AppCompatActivity(), GetDataSnapshots.listener {
    var database = FirebaseDatabase.getInstance()
    var dataSnapshots = mutableListOf<DataSnapshot>()
    var idList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        GetDataSnapshots(this, "coin")

        btn_admin_save.setOnClickListener {
            val size = Integer.valueOf(edit_admin_range_to.text.toString())
            var area = 0
            while (area < size) {
                val myRef = database.getReference("coin/" + (area + 1) + "," + (area + COIN_VALUE))
                myRef.push()
                dataSnapshots
                        .map { it.getValue(String::class.java) }
                        .filterNot { idList.contains(it) }
                        .forEach { idList.add(it) }
                myRef.setValue(ANDROID)
                area += COIN_VALUE
            }
            finish()
        }
        btn_admin_return.setOnClickListener {
            finish()
        }
        btn_admin_cleardata.setOnClickListener {
            val sp = getPreferences(Context.MODE_PRIVATE)
            sp.edit().clear().apply()
            toast(this, "clear")
        }
    }

    override fun onDataSnapshot(_dataSnapshots: ArrayList<DataSnapshot>) {
        dataSnapshots = _dataSnapshots
    }
}
