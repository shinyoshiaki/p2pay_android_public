package com.example.sysn.multipeerconnection.money

import android.os.Handler
import com.example.sysn.multipeerconnection.activity.home.pay.ReceiveRemoteActivity
import com.example.sysn.multipeerconnection.etc.A.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.etc.F.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by shiny on 2017/12/19.
 */
class HelperWallet {

    class Transaction constructor(val id: String, val value: String)

    val myTransactions = MyTransactions()
    val myTranIds = MyTranIds()

    init {
        if (save) {
            myTransactions.load()
            myTranIds.load()
        }
    }

    class MyTransactions {
        var v = ArrayList<Transaction>()

        private fun isIdExist(_id: String): Boolean = v.any { it.id == _id }

        fun add(_id: String, _value: String) {
            if (!isIdExist(_id)) {
                v.add(Transaction(_id, _value))
            }
            if (save) save()
        }

        fun getTransaction(_id: String): String? {
            for (tran in v) {
                if (tran.id == _id) {
                    return tran.value
                }
            }
            return null
        }

        fun save() {
            JSONArray().let {
                for (tran in v) {
                    it.put(JSONObject()
                            .put("id", tran.id)
                            .put("value", tran.value)
                            .toString())
                }
                saveJSONArray("myTransactions", it, mainActivity)
            }
        }

        fun load() {
            v = ArrayList<Transaction>()
            loadJSONArray("myTransactions", mainActivity)?.let {
                for (i in 0 until it.length()) {
                    try {
                        it.getJSONObject(i).let {
                            val id = it.getString("id")
                            val value = it.getString("value")
                            v.add(Transaction(id, value))
                        }
                    } catch (e: JSONException) {
                    }
                }
            }
        }
    }

    class MyTranIds {
        var v = ArrayList<String>()

        fun add(str: String) {
            v.add(str)
            save()
        }

        fun save() {
            JSONArray().let {
                for (tran in v) {
                    it.put(JSONObject()
                            .put("tran_id", tran))
                }
                saveJSONArray("tran_ids", it, mainActivity)
            }
        }

        fun load() {
            v = ArrayList<String>()
            loadJSONArray("tran_ids", mainActivity)?.let {
                for (i in 0 until it.length()) {
                    try {
                        it.getJSONObject(i).let {
                            v.add(it.getString("tran_id"))
                        }
                    } catch (e: JSONException) {
                        log("wallet myKeys load json parse error")
                    }
                }
            }
        }
    }

    fun receive(arr: JSONArray) {
        for (i in 0 until arr.length()) {
            try {
                arr.getJSONObject(i).let {
                    log(it.toString())
                    val key = it.getString("key")
                    val coin = it.getString("coin")
                    wallet.receive(coin, key)
                }
            } catch (e: JSONException) {
                log("receive remote json parse error fun receive")
            }
        }
    }

    fun receiveRemote(arr: JSONArray, tranId: String) {
        receive(arr)
        myTranIds.add(tranId)
    }

    fun receiveLocal(json: JSONObject) {

        val from = json.getString("from")

        class Coin_Key constructor(val coin: String, val key: String)

        val coin_key = ArrayList<Coin_Key>()

        JSONArray(json.getString("coins")).let {
            for (i in 0 until it.length()) {
                it.getJSONObject(i).let {
                    coin_key.add(Coin_Key(it.getString("coin"), it.getString("key")))
                }
            }
        }


        val tranId = json.getString("tran_id")
        myTranIds.add(tranId)

        JSONArray().let {
            for (v in coin_key) {
                it.put(JSONObject().put("coin", v.coin))
            }

            val packet = JSONObject()
                    .put(ORDER, "pay")
                    .put("pay", from)
                    .put("receive", ANDROID)
                    .put("tran_id", tranId)
                    .put("coins", it.toString())

            if (manager.getReader() == ANDROID) {
                connect.shareJson(blockChain.getSendBlockchain(packet))
            } else {
                connect.shareJson(blockChain.getSendTransaction(packet))
            }

            Handler().postDelayed({
                if (blockChain.blocks2JsonArray().toString().contains(tranId)) {
                    receive(JSONArray(json.getString("coins")))
                }
            }, 3000)
        }

    }
}