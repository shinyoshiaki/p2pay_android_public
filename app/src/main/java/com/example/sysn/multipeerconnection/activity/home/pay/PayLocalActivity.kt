package com.example.sysn.multipeerconnection.activity.home.pay

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.activity.ReadQrActivity
import com.example.sysn.multipeerconnection.etc.A.*
import kotlinx.android.synthetic.main.activity_paylocal_pay.view.*
import kotlinx.android.synthetic.main.activity_paylocal_receive.view.*
import kotlinx.android.synthetic.main.common_activity_tablayout.*
import org.json.JSONObject
import shinyoshiaki.blockchain.etc.F.log

/**
 * Created by shiny on 2017/10/26.
 */
class PayLocalActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {
    val PAGENUM = 2
    var activity: Activity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_activity_tablayout)

        val adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getItem(position: Int): Fragment = TestFragment.newInstance(position + 1)

            override fun getPageTitle(position: Int): CharSequence {
                when (position + 1) {
                    1 -> return "支払い"
                    2 -> return "受け取り"
                }
                return "error"
            }

            override fun getCount(): Int = PAGENUM
        }
        pager.adapter = adapter
        pager.addOnPageChangeListener(this)
        tabs.setupWithViewPager(pager)
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        when (position + 1) {
            1 -> {
            }
            2 -> {
                startActivity(Intent(applicationContext, ReadQrActivity::class.java))
            }
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}

    class TestFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            var view: View? = null
            when (arguments.getInt("page", 0)) {
                1 -> {
                    view = inflater!!.inflate(R.layout.activity_paylocal_pay, container, false)
                    Pay(activity, view)
                }
                2 -> {
                    view = inflater!!.inflate(R.layout.activity_paylocal_receive, container, false)
                    Receive(activity, view)
                }
            }
            return view
        }

        companion object {
            fun newInstance(page: Int): TestFragment {
                val args = Bundle()
                args.putInt("page", page)
                val fragment = TestFragment()
                fragment.arguments = args
                return fragment
            }
        }
    }

    class Pay(activity: Activity, view: View) {
        init {
            view.btn_paylocalpay_pay.setOnClickListener {
                wallet.pay.localPay(view.edit_paylocalpay.text.toString().toInt())?.let {
                    //pay local
                    util.display(it.toString(), view.image_paylocalpay, activity)
                    helperWallet.myTranIds.add(it.getString("tran_id"))
                }
            }
        }
    }

    class Receive(activity: Activity, view: View) {
        init {
            val handler = Handler()
            Handler().post(object : Runnable {
                override fun run() {
                    ReadQrActivity.result.let {
                        view.text_paylocalreceive.text = it
                    }
                    handler.postDelayed(this, 1000)
                }
            })

            view.btn_paylocalreceive.setOnClickListener {
                ReadQrActivity.result.let {
                    log("payLocal receive:" + it)

                    helperWallet.receiveLocal(JSONObject(it))

                    val dialog = util.progress("決済処理中", activity)
                    Handler().postDelayed({
                        dialog.dismiss()

                        if (wallet.isTransactionSuccess()) {
                            activity.finish()
                        } else {
                            util.msgWindow(activity, "エラー", "決済失敗")
                            Handler().postDelayed({ activity.finish() }, 3000)
                        }

                    }, 3500)
                }
            }
        }
    }
}