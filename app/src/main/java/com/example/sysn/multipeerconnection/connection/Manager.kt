package com.example.sysn.multipeerconnection.connection

import android.os.Handler
import com.example.sysn.multipeerconnection.etc.A.*
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.blockchain.PBFT
import shinyoshiaki.blockchain.etc.F.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

/**
 * Created by shiny on 2017/12/08.
 */
class Manager(_connect: Connect, _pbft: PBFT, _listener: Manager.Listener) : Connect.Listener, PBFT.Listener {
    var listener = _listener

    fun getReader(): String? {
        val devices = connect.devices.v.clone() as ArrayList<Connect.Devices.Device>
        if (devices.size <= 0) return null

        class DateComparator : Comparator<Connect.Devices.Device> {
            override fun compare(a: Connect.Devices.Device, b: Connect.Devices.Device): Int {
                val no1 = string2date(a.time)
                val no2 = string2date(b.time)
                return no1.compareTo(no2)
            }
        }

        devices.add(Connect.Devices.Device(ANDROID, date2string(TIME_LOGIN)))
        Collections.sort(devices, DateComparator())

        return devices[0].id
    }

    var isReaderLive = true
    var isCheckingLive = false

    init {
        _connect.listener = this
        _pbft.listener = this

        val reader = Handler()
        Handler().post(object : Runnable {
            override fun run() {
                if (connect.peers.connectedNum() > 0)
                    if (getReader() == ANDROID) {
                        //log("send reader live")
                        connect.shareJson(JSONObject().put(ORDER, "reader"))
                    }

                reader.postDelayed(this, 1000)
            }
        })
        val check = Handler()
        Handler().post(object : Runnable {
            override fun run() {
                if (connect.peers.connectedNum() > 0)
                    if (getReader() != ANDROID) {
                        if (isReaderLive) {
                            isReaderLive = false
                        } else if (!isCheckingLive) {
                            log("isCheckingLive")
                            isCheckingLive = true

                            var type = ""
                            val peer: Connect.Peer?
                            if (connect.peers.offer.find { it!!.target() == getReader() } != null) {
                                type = connect.PEERSO
                                peer = connect.peers.offer.find { it!!.target() == getReader() }
                            } else {
                                type = connect.PEERSA
                                peer = connect.peers.answer.find { it!!.target() == getReader() }
                            }
                            peer?.let {
                                connect.sendPing(type, it.label)
                                Handler().postDelayed({
                                    if (!it.isConnected) {
                                        isCheckingLive = false
                                        log("reader dead: " + getReader())
                                        getReader()?.let {
                                            connect.devices.remove(it)
                                        }
                                    }
                                }, (5 * 1000).toLong())
                            }
                        }
                    }
                check.postDelayed(this, 2000)
            }
        })

        val debug = Handler()
        Handler().post(object : Runnable {
            override fun run() {
                log("get reader: ${getReader()}")

                debug.postDelayed(this, 10000)
            }
        })
    }

    override fun onConnectCommand(value: String) {
        listener.onManager(value)

        try {
            JSONObject(value).let {
                when (it.getString(ORDER)) {
                    TRANSACTION -> if (getReader() == ANDROID) pbft.addTransaction(it.getString(TRANSACTION))
                    BLOCKCHAIN -> blockChain.replaceChain(it.getString(BLOCKCHAIN))
                    "reader" -> {
                        isReaderLive = true
                        //log("reader received")
                    }
                }
            }
        } catch (ignored: JSONException) {
        }
    }

    interface Listener : EventListener {
        fun onManager(value: String)
    }

    override fun onPbft(value: String) {
        log("onPbft")
        connect.shareJson(JSONObject(value))
    }
}