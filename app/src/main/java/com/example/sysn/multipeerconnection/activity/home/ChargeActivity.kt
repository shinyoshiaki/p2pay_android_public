package com.example.sysn.multipeerconnection.activity.home

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.etc.A.*
import com.example.sysn.multipeerconnection.money.GetDataSnapshots
import com.google.firebase.database.DataSnapshot
import kotlinx.android.synthetic.main.activity_charge.*
import shinyoshiaki.blockchain.etc.F
import shinyoshiaki.blockchain.etc.F.sha256
import shinyoshiaki.blockchain.etc.F.toast
import java.util.*

/**
 * Created by shiny on 2017/09/20.
 */

class ChargeActivity : AppCompatActivity(), GetDataSnapshots.listener {
    internal var activity: Activity = this
    internal var dataSnapshots = ArrayList<DataSnapshot>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_charge)
        GetDataSnapshots(this, "coin")

        wallet.refresh()

        val handler = Handler()
        handler.post(object : Runnable {
            override fun run() {
                text_charge_limit.text = dataSnapshots.size.toString() + "口"
                handler.postDelayed(this, 500)
            }
        })

        btn_charge_charge.setOnClickListener {
            val size = Integer.valueOf(edit_charge_charge.text.toString())
            if (size <= dataSnapshots.size) {
                for ((i, dataSnapshot) in dataSnapshots.withIndex()) {
                    if (i >= size) break
                    val from = Integer.valueOf(dataSnapshot.key.toString().split(",")[0])
                    val to = Integer.valueOf(dataSnapshot.key.toString().split(",")[1])
                    val id = "$from,$to"

                    wallet.registBlockchain(from, to, id)

                    val dialog = util.progress("決済処理中", this)
                    Handler().postDelayed({
                        dialog.dismiss()
                        if (wallet.isTransactionSuccess()) {
                            dataSnapshot.ref.removeValue()
                            finish()
                        } else {
                            util.msgWindow(this, "エラー", "決済失敗")
                            Handler().postDelayed({ finish() }, 3000)
                        }
                    }, 3500)
                }
            } else {
                toast(activity, "error")
            }
        }
        btn_charge_return.setOnClickListener {
            finish()
        }
    }

    override fun onDataSnapshot(_dataSnapshots: ArrayList<DataSnapshot>) {
        dataSnapshots = _dataSnapshots
    }
}
