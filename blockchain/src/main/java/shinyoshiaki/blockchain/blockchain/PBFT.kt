package shinyoshiaki.blockchain.blockchain

import android.os.Handler
import android.util.Log
import org.json.JSONObject
import java.util.*

/**
 * Created by shiny on 2017/12/08.
 */

class PBFT {
    val PASE = 8.toLong()
    val transactions = ArrayList<String>()
    val blockchain = BlockChain()
    var listener: PBFT.Listener? = null

    fun addTransaction(str: String) {
        if (!transactions.contains(str)) transactions.add(str)
    }

    init {
        val handler = Handler()
        Handler().post(object : Runnable {
            override fun run() {
                consent()
                handler.postDelayed(this, 1000 / PASE)
            }
        })
    }

    fun consent() {
        if (transactions.size > 0) {
            Log.d("log", "consent:" + transactions[0])

            listener?.onPbft(blockchain.getSendBlockchain(JSONObject(transactions[0])).toString())
            transactions.removeAt(0)
        }
    }

    interface Listener : EventListener {
        fun onPbft(value: String)
    }
}