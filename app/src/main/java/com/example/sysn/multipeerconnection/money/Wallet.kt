package com.example.sysn.multipeerconnection.money

import com.example.sysn.multipeerconnection.etc.A
import com.example.sysn.multipeerconnection.etc.A.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.blockchain.Block
import shinyoshiaki.blockchain.blockchain.BlockChain
import shinyoshiaki.blockchain.blockchain.BlockChain.Companion.blockchain
import shinyoshiaki.blockchain.etc.F.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by shiny on 2017/09/20.
 */

class Wallet {
    val blockChain = BlockChain()
    val myKeys = MyKeys()
    val myCoins = MyCoins()
    val pay = Pay()
    private var tranId = ""

    init {
        if (save) {
            myKeys.load()
        }
    }

    fun json2coin(json: JSONObject): Coin? {
        return try {
            Coin(json.getInt("rangeFrom"),
                    json.getInt("rangeEnd"),
                    json.getString("id"),
                    json.getString("sign"))
        } catch (e: JSONException) {
            null
        }
    }

    class Coin constructor(var rangeFrom: Int, var rangeEnd: Int, var id: String, var sign: String) {
        fun value(): Int = rangeEnd - rangeFrom + 1

        fun toJson(): JSONObject {
            return JSONObject().put("rangeFrom", rangeFrom)
                    .put("rangeEnd", rangeEnd)
                    .put("id", id)
                    .put("sign", sign)
        }
    }

    class MyCoins {
        val v = ArrayList<Coin>()

        fun total(): Int = v.sumBy { it.value() }
    }

    class MyKey constructor(val coinId: String, val coinKey: String)

    class MyKeys {
        private var keys = ArrayList<MyKey>()

        fun isRightCoinKey(_coin: Coin, _key: String): Boolean = (aesEncrypt.decy(_coin.sign, _key) == _coin.id)

        fun add(_id: String, _key: String) {
            var exist = false
            keys.forEach { myKey ->
                if (_id == myKey.coinId) {
                    exist = true
                    return@forEach
                }
            }
            if (!exist) {
                keys.add(MyKey(_id, _key))
                save()
            }
        }

        fun delete(_id: String) {
            for ((i, myKey) in keys.withIndex()) {
                if (myKey.coinId == _id) {
                    keys.removeAt(i)
                    break
                }
            }
        }

        fun getKey(_id: String): String? {
            keys.forEach { it -> if (it.coinId == _id) return it.coinKey }
            return null
        }

        fun save() {
            JSONArray().let {
                for (key in keys) {
                    it.put(JSONObject()
                            .put("id", key.coinId)
                            .put("key", key.coinKey))
                }
                saveJSONArray("myKeys", it, mainActivity)
            }
        }

        fun load() {
            keys = ArrayList<MyKey>()
            loadJSONArray("myKeys", mainActivity)?.let {
                for (i in 0 until it.length()) {
                    try {
                        it.getJSONObject(i).let {
                            val id = it.getString("id")
                            val key = it.getString("key")
                            keys.add(MyKey(id, key))
                        }
                    } catch (e: JSONException) {
                        log("wallet myKeys load json parse error")
                    }
                }
            }
        }

        fun checkCoin(coin: Coin): Boolean {
            for (key in keys) {
                if (isRightCoinKey(coin, key.coinKey)) return true
            }
            return false
        }
    }

    fun registBlockchain(_rangeFrom: Int, _rangeEnd: Int, _id: String) {
        val getRandomKey = aesEncrypt.getString(5)
        val coin = Coin(_rangeFrom, _rangeEnd, _id, aesEncrypt.ency(_id, getRandomKey))
        val packet = JSONObject()
                .put(ORDER, "regist")
                .put("user", ANDROID)
                .put("coin", coin.toJson().toString())
        if (manager.getReader() == ANDROID) {
            connect.shareJson(blockChain.getSendBlockchain(packet))
        } else {
            connect.shareJson(blockChain.getSendTransaction(packet))
        }

        myKeys.add(coin.id, getRandomKey)
    }

    inner class Pay {
        private var paiedCoins = ArrayList<Coin>()

        private fun splitCoin(last: Coin, from: Int, to: Int) {
            if (!myKeys.getKey(last.id).isNullOrEmpty()) {
                val id = "$from,$to"
                val key = aesEncrypt.getString(5)
                val lastCoin = Coin(from, to, id, aesEncrypt.ency(id, key))
                if (lastCoin.value() > 0) {
                    paiedCoins.add(lastCoin)
                    myKeys.add(id, key)
                }
            }
        }

        private fun ready(value: Int): Boolean {
            tranId = (Date().time / 1000).toString() + ANDROID      //make transaction id
            paiedCoins = ArrayList<Coin>()
            refresh()

            if (value > myCoins.total()) {
                return false
            }

            var buf = 0
            var max = 0
            for ((i, coin) in myCoins.v.withIndex()) {
                if (myKeys.getKey(coin.id).isNullOrEmpty()) {
                    log("wallet valid key")
                    continue
                }
                buf += coin.value()
                if (buf >= value) {
                    break
                } else {
                    paiedCoins.add(coin)
                }
                max = i + 1
            }
            if (max >= myCoins.v.size) {
                log("wallet error over max")
            } else {
                val last = myCoins.v[max]
                val from = last.rangeFrom
                val to = last.rangeEnd - (buf - value)
                splitCoin(last, from, to)
            }

            return true
        }

        fun remotePay(target: String, value: Int): JSONObject? {
            if (ready(value)) {
                JSONArray().let {
                    for (coin in paiedCoins) {
                        it.put(JSONObject().put("coin", coin.toJson().toString()))
                    }

                    val packet = JSONObject()
                            .put(ORDER, "pay")
                            .put("pay", ANDROID)
                            .put("receive", target)
                            .put("tran_id", tranId)
                            .put("coins", it.toString())

                    if (manager.getReader() == ANDROID) {
                        connect.shareJson(blockChain.getSendBlockchain(packet))
                    } else {
                        connect.shareJson(blockChain.getSendTransaction(packet))
                    }
                }

                JSONArray().let {
                    for (coin in paiedCoins) {
                        it.put(JSONObject()
                                .put("coin", coin.toJson().toString())
                                .put("key", myKeys.getKey(coin.id)))
                        myKeys.delete(coin.id)
                    }

                    return JSONObject()
                            .put("tran_id",tranId)
                            .put("coins",it)
                }
            } else
                return null
        }

    fun localPay(value: Int):JSONObject? {
            if (ready(value)) {
                JSONArray().let {
                    for (coin in paiedCoins) {
                        it.put(JSONObject()
                                .put("coin", coin.toJson().toString())
                                .put("key", myKeys.getKey(coin.id)))
                        myKeys.delete(coin.id)
                    }
                    return JSONObject()
                            .put("from", ANDROID)
                            .put("coins", it.toString())
                            .put("tran_id", tranId)
                }
            } else return null
        }
    }

    fun receive(_coin: String, _key: String) {
        log("wallet receive")
        log("coin: " + _coin)
        log("key: " + _key)
        json2coin(JSONObject(_coin))?.let {
            if (myKeys.isRightCoinKey(it, _key)) {
                val key = aesEncrypt.getString(5)
                it.sign = aesEncrypt.ency(it.id, key)

                val packet = JSONObject()
                        .put(ORDER, "receive")
                        .put("receive", ANDROID)
                        .put("coin", it.toJson().toString())

                if (manager.getReader() == ANDROID) {
                    connect.shareJson(blockChain.getSendBlockchain(packet))
                } else {
                    connect.shareJson(blockChain.getSendTransaction(packet))
                }

                myKeys.add(it.id, key)
            }
        }
    }

    fun refresh() {
        myCoins.v.clear()
        val clone = blockchain.clone() as ArrayList<Block>
        for (block in clone) {
            try {
                val json = JSONObject(block.data)
                val order = json.getString(ORDER)

                when (order) {
                    "regist" -> {
                        if (json.getString("user") == ANDROID) {
                            json2coin(JSONObject(json.getString("coin")))?.let {
                                myCoins.v.add(it)
                            }
                        }
                    }
                    "pay" -> {
                        if (json.getString("pay") == ANDROID) {
                            JSONArray(json.getString("coins")).let {
                                for (i in 0 until it.length()) {
                                    it.getJSONObject(i).let {
                                        json2coin(JSONObject(it.getString("coin")))?.let {
                                            myCoins.v
                                                    .filter { coins_ -> coins_.rangeFrom <= it.rangeFrom && it.rangeEnd <= coins_.rangeEnd }
                                                    .forEach { coins_ -> coins_.rangeFrom = it.rangeEnd + 1 }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    "receive" -> {
                        json2coin(JSONObject(json.getString("coin")))?.let {
                            if (json.getString("receive") == ANDROID) myCoins.v.add(it)
                        }
                    }
                }
            } catch (e: JSONException) {
            }
        }
    }

    fun isTransactionSuccess(): Boolean = blockChain.blocks2JsonArray().toString().contains(wallet.tranId)

}
