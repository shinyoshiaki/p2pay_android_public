package shinyoshiaki.blockchain.webrtc;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.EventListener;

import shinyoshiaki.blockchain.etc.F;

import static android.content.ContentValues.TAG;

public class WebRTC implements PeerConnection.Observer, SdpObserver, DataChannel.Observer {
    private PeerConnectionFactory peerConnectionFactory;
    private PeerConnection peerConnection;
    private DataChannel dataChannel;

    private ArrayList<PeerConnection.IceServer> iceServers = new ArrayList<>();

    private Listener listener = null;
    private int peerId = 0;
    public String target = null;

    public static void initWebRTC(Activity _activity) {
        PeerConnectionFactory.initializeAndroidGlobals(_activity, true, true, true);
    }

    public WebRTC(Listener _listener) {
        listener = _listener;
        peerConnectionFactory = new PeerConnectionFactory();
        iceServers.add(new PeerConnection.IceServer("stun:stun.l.google.com:19302"));
    }


    public void initOffer(String... args) {
        peerConnection = peerConnectionFactory.createPeerConnection(iceServers, new MediaConstraints(), this);
        dataChannel = peerConnection.createDataChannel("commands" + peerId, new DataChannel.Init());

        peerConnection.createOffer(this, new MediaConstraints());
        options = args;
    }

    private String options[] = new String[100];

    public void initAnswer(String... args) {
        peerConnection = peerConnectionFactory.createPeerConnection(iceServers, new MediaConstraints(), this);
        dataChannel = peerConnection.createDataChannel("commands" + peerId, new DataChannel.Init());

        options = args;
    }

    public void receiveOffer(String... args) { //rtcOffer do this
        if (args.length > 1) target = args[1];
        try {
            JSONObject obj = new JSONObject(args[0]);
            SessionDescription sdp = new SessionDescription(SessionDescription.Type.ANSWER, obj.getString("sdp"));
            peerConnection.setRemoteDescription(this, sdp);
        } catch (JSONException ignored) {
        }
    }

    public void receiveAnswer(String... args) { //rtcAnswer do this
        if (args.length > 1) target = args[1];
        try {
            JSONObject obj = new JSONObject(args[0]);
            SessionDescription sdp = new SessionDescription(SessionDescription.Type.OFFER, obj.getString("sdp"));
            peerConnection.setRemoteDescription(this, sdp);
        } catch (JSONException ignored) {
        }
        peerConnection.createAnswer(this, new MediaConstraints());
    }

    public void sendThroughDataChannel(String command) {
        if (dataChannel != null)
            dataChannel.send(new DataChannel.Buffer(ByteBuffer.wrap(command.getBytes(Charset.forName("UTF-8"))), false));
    }

    public boolean peerConnectionExist() {
        return peerConnection != null;
    }

    public boolean targetExist() {
        return target != null;
    }

    // PeerConnection.Observer -----
    @Override
    public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
        if (iceGatheringState == PeerConnection.IceGatheringState.COMPLETE) {
            SessionDescription sessionDescription = peerConnection.getLocalDescription();

            if (sessionDescription == null) {
                F.log("onIceGathering error");
                return;
            }

            JSONObject sdp = new JSONObject();
            try {
                sdp.put("type", sessionDescription.type.toString().toLowerCase());
                sdp.put("sdp", sessionDescription.description);
            } catch (JSONException ignored) {
            }

            if (sessionDescription.type.toString().toLowerCase().equals("offer")) {
                listener.onSdpOffer(sdp.toString(), options);
            } else {
                listener.onSdpAnswer(sdp.toString(), options);
            }
        }
    }

    @Override
    public void onDataChannel(DataChannel dataChannel) {
        dataChannel.registerObserver(this);
    }

    @Override
    public void onIceConnectionReceivingChange(boolean b) {
    }

    @Override
    public void onSignalingChange(PeerConnection.SignalingState signalingState) {
    }

    @Override
    public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
    }

    @Override
    public void onIceCandidate(IceCandidate iceCandidate) {
    }

    @Override
    public void onAddStream(MediaStream mediaStream) {
    }

    @Override
    public void onRemoveStream(MediaStream mediaStream) {
    }

    @Override
    public void onRenegotiationNeeded() {
    }

    // SdpObserver -----
    @Override
    public void onCreateSuccess(SessionDescription sessionDescription) {
        peerConnection.setLocalDescription(this, sessionDescription);
    }

    @Override
    public void onSetSuccess() {
    }

    @Override
    public void onCreateFailure(String s) {
    }

    @Override
    public void onSetFailure(String s) {
    }

    // DataChannel.Observer -----
    @Override
    public void onMessage(final DataChannel.Buffer buffer) {
        byte[] msgAsByteArray = new byte[buffer.data.capacity()];
        buffer.data.get(msgAsByteArray);
        String command = new String(msgAsByteArray, Charset.forName("UTF-8"));
        listener.onCommandChanged(command);
    }

    @Override
    public void onBufferedAmountChange(long l) {
    }

    @Override
    public void onStateChange() {
    }

    public interface Listener extends EventListener {
        void onCommandChanged(final String value);

        void onSdpAnswer(final String sdp, final String[] args);

        void onSdpOffer(final String sdp, final String[] args);
    }
}
