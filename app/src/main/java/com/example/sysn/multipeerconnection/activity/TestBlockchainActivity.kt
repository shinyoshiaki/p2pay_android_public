package com.example.sysn.multipeerconnection.activity

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.connection.Manager
import com.example.sysn.multipeerconnection.etc.A.*
import kotlinx.android.synthetic.main.activity_test_sharing.*
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.blockchain.Block
import shinyoshiaki.blockchain.blockchain.BlockChain.Companion.blockchain
import shinyoshiaki.blockchain.etc.F.setText

/**
 * Created by shiny on 2017/09/21.
 */

class TestBlockchainActivity : AppCompatActivity(), Manager.Listener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_sharing)

        manager.listener=this

        share_txt.setTextIsSelectable(true)
        share_edit.setText("")
        share_txt_id.text = ANDROID

        share_text_chain.setTextIsSelectable(true)
        listBlockchain()

        val handler = Handler()
        Handler().post(object : Runnable {
            override fun run() {
                share_text_peer.text = ""
                for ((i, peer) in connect.peers.answer.withIndex())
                    if (peer!!.targetExist())
                        setText("peersA(" + i + "):" + peer.rtc.target, share_text_peer)
                for ((i, peer) in connect.peers.offer.withIndex())
                    if (peer!!.targetExist())
                        setText("peersO(" + i + "):" + peer.rtc.target, share_text_peer)

                handler.postDelayed(this, 1000)
            }
        })
        share_btn_move.setOnClickListener {
            finish()
        }
        share_btn.setOnClickListener {
            val string = share_edit.text.toString()
            share_txt.text = string
            val packet = JSONObject()
                    .put(ORDER, "chat")
                    .put("sender", ANDROID)
                    .put("value", string)
            connect.shareJson(packet)
        }
    }

    override fun onManager(value: String) {
        runOnUiThread {
            try {
                when (JSONObject(value).getString(ORDER)) {
                    "chat" -> share_txt.text = value
                }
            } catch (e: JSONException) {
            }

            Handler().postDelayed({ listBlockchain() }, (1000 / 2).toLong())
        }
    }

    fun listBlockchain() {
        share_text_chain.text = ""
        val chain = blockchain.clone() as ArrayList<Block>
        for (block in chain) setText(block.data, share_text_chain)
    }
}
