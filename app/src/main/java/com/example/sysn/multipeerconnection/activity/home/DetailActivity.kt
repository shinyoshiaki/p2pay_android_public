package com.example.sysn.multipeerconnection.activity.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.etc.A.wallet
import kotlinx.android.synthetic.main.activity_detail.*
import shinyoshiaki.blockchain.etc.F.toast

/**
 * Created by shiny on 2017/09/20.
 */

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        wallet.refresh()

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1)

        for (coin in wallet.myCoins.v)
            if (coin.value() > 0)
                adapter.add(coin.rangeFrom.toString() + "～" + coin.rangeEnd + "\n" + coin.value() + " \nid:" + coin.id + "\nsign:" + coin.sign + "\n")

        list_detail_detail.adapter = adapter
        list_detail_detail.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            val list_detail_detail = parent as ListView
            val item = list_detail_detail.getItemAtPosition(position) as String
            toast(this, item)
        }
        btn_detail_return.setOnClickListener {
            finish()
        }
    }

}

