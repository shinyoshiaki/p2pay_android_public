package shinyoshiaki.blockchain.blockchain

import org.json.JSONObject

/**
 * Created by shiny on 2017/08/19.
 */

class Block(var index: Int, var previousHash: String, var timestamp: String, var data: String, var hash: String) {

    fun toJson(): JSONObject {
        return JSONObject()
                .put("index", index)
                .put("previousHash", previousHash)
                .put("timestamp", timestamp)
                .put("data", data)
                .put("hash", hash)
    }

}
