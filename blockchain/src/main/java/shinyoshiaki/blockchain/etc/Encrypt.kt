package shinyoshiaki.blockchain.etc

import android.util.Base64
import org.apache.commons.lang3.RandomStringUtils
import java.io.ByteArrayOutputStream
import java.io.UnsupportedEncodingException
import java.security.SecureRandom
import java.util.*
import java.util.zip.Deflater
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

/**
 * Created by shiny on 2017/11/21.
 */
class AESEncrypt {
    var secKey: SecretKeySpec? = null
    var keys: ByteArray? = null
    var secretKey: String? = null

    fun getString(size: Int): String = RandomStringUtils.randomAlphabetic(size)

    private fun genKey(_secretkey: String) {
        secretKey = _secretkey
        val bytes = ByteArray(128 / 8)
        try {
            keys = _secretkey.toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        (0 until _secretkey.length)
                .takeWhile { it < bytes.size }
                .forEach { bytes[it] = keys!![it] }
        secKey = SecretKeySpec(bytes, "AES")
    }

    fun ency(en: String, _key: String): String {
        genKey(_key)
        var de: ByteArray? = null
        try {
            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.ENCRYPT_MODE, secKey)
            de = cipher.doFinal(en.toByteArray(charset("UTF-8")))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return Base64.encodeToString(de, Base64.DEFAULT)
    }

    fun decy(de: String, _key: String): String {
        genKey(_key)
        var en: ByteArray? = null
        try {
            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.DECRYPT_MODE, secKey)
            en = cipher.doFinal(Base64.decode(de.toByteArray(charset("UTF-8")), Base64.DEFAULT))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return String(en!!)
    }

}