package shinyoshiaki.blockchain.blockchain

import android.app.Activity
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.etc.F.*
import java.util.*

/**
 * Created by yoshiaki on 2017/08/15.
 */

class BlockChain {

    private val genesisBlock: Block
        get() = Block(0, "0", 1465154705.toString(), "my genesis block!!", "816534932c2b7154836da6afc367695e6337db8a921823784c14378abed4f7d7")

    init {
        if (blockchain.size == 0)
            blockchain.add(genesisBlock)
    }

    fun generateNextBlock(_data: String) {
        val nextIndex = blockchain.size
        val previousBlock = blockchain[blockchain.size - 1]
        val nextTimestamp = (Date().time / 1000).toString()
        val nexthash = sha256(nextIndex.toString() + previousBlock.hash + nextTimestamp + _data)
        val newBlock = Block(nextIndex, previousBlock.hash, nextTimestamp, _data, nexthash!!)
        if (isValidNewBlock(newBlock, blockchain[blockchain.size - 1])) {
            blockchain.add(newBlock)
        }
    }

    private fun isValidNewBlock(newBlock: Block, previousBlock: Block): Boolean {
        if (previousBlock.index + 1 != newBlock.index) {
            println("invalid index")
            return false
        } else if (previousBlock.hash != newBlock.previousHash) {
            println("invalid previoushash")
            return false
        } else if (sha256(newBlock.index.toString() + newBlock.previousHash + newBlock.timestamp + newBlock.data) != newBlock.hash) {
            println("invalid hash")
            return false
        }
        return true
    }

    fun replaceChain(jsons: String) {
        val newBlocks = ArrayList<Block>()
        try {
            JSONArray(jsons).let {
                for (i in 0 until it.length()) {
                    val json = it.getJSONObject(i)
                    newBlocks.add(Block(
                            json.getInt("index"),
                            json.getString("previousHash"),
                            json.getString("timestamp"),
                            json.getString("data"),
                            json.getString("hash")
                    ))
                }
                if (isValidChain(newBlocks) && newBlocks.size > blockchain.size) {
                    blockchain = newBlocks
                }
            }
        } catch (e: JSONException) {
        }
    }

    private fun isValidChain(blockchainToValidate: ArrayList<Block>): Boolean {
        if (blockchainToValidate[0].toJson().toString() != genesisBlock.toJson().toString()) {
            return false
        }
        for (i in 1 until blockchainToValidate.size) {
            if (!isValidNewBlock(blockchainToValidate[i], blockchainToValidate[i - 1])) {
                return false
            }
        }
        return true
    }

    ////////////////////////////////////////////////////////////////////////////
    fun blocks2JsonArray(): JSONArray {
        JSONArray().let {
            val clone = blockchain.clone() as ArrayList<Block>
            for (block in clone)
                it.put(block.toJson())
            return it
        }
    }

    fun jsonArray2blocks(arr: JSONArray): ArrayList<Block> {
        val chain = ArrayList<Block>()
        try {
            for (i in 0 until arr.length()) {
                val json = arr.getJSONObject(i)
                chain.add(Block(
                        json.getInt("index"),
                        json.getString("previousHash"),
                        json.getString("timestamp"),
                        json.getString("data"),
                        json.getString("hash")
                ))
            }
        } catch (ignored: JSONException) {
        }
        return chain
    }

    fun saveBlockchain(activity: Activity) {
        activity.runOnUiThread { saveJSONArray("BLOCKCHAIN", blocks2JsonArray(), activity) }
    }

    fun loadBlockchain(activity: Activity) {
        activity.runOnUiThread {
            loadJSONArray("BLOCKCHAIN", activity)?.let {
                blockchain = jsonArray2blocks(it)
            }
        }
    }

    fun getSendTransaction(packet: JSONObject): JSONObject {
        return JSONObject()
                .put("ORDER", "TRANSACTION")
                .put("TRANSACTION", packet)
    }

    fun getSendBlockchain(packet: JSONObject): JSONObject {
        generateNextBlock(packet.toString())
        return JSONObject()
                .put("ORDER", "BLOCKCHAIN")
                .put("BLOCKCHAIN", blocks2JsonArray().toString())
                .put("TRANSACTION", packet)
    }

    companion object {
        var blockchain = ArrayList<Block>()
    }
}