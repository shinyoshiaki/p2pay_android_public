package com.example.sysn.multipeerconnection.etc;

import android.os.AsyncTask;

import com.example.sysn.multipeerconnection.activity.MainActivity;
import com.example.sysn.multipeerconnection.connection.Connect;
import com.example.sysn.multipeerconnection.connection.Manager;
import com.example.sysn.multipeerconnection.money.HelperWallet;
import com.example.sysn.multipeerconnection.money.Wallet;

import org.apache.commons.net.io.Util;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import shinyoshiaki.blockchain.blockchain.BlockChain;
import shinyoshiaki.blockchain.blockchain.PBFT;
import shinyoshiaki.blockchain.etc.AESEncrypt;
import shinyoshiaki.blockchain.webrtc.WebRTC;

/**
 * Created by yoshiaki on 2017/08/11.
 */

public class A {
    public static final String AID = "AID", MSGID = "MSGID", FIRST = "FIRST", MULTI = "MULTI", FINAL = "FINAL", ORDER = "ORDER",
            TRANSACTION = "TRANSACTION", BLOCKCHAIN = "BLOCKCHAIN";

    public static boolean save = false;

    public static Connect connect = null;
    public static Manager manager = null;
    public static AESEncrypt aesEncrypt = null;
    public static Wallet wallet = null;
    public static MainActivity mainActivity = null;
    public static com.example.sysn.multipeerconnection.etc.Util util = null;
    public static PBFT pbft = null;
    public static BlockChain blockChain = null;
    public static HelperWallet helperWallet=null;

    public static String ANDROID = null;
    public static Date TIME_LOGIN = null;
    public static int COIN_VALUE = 1000;

    public static class msgId {
        public static ArrayList<String> list = new ArrayList<>();

        public static String get() {
            String id;
            while (true) {
                id = String.valueOf(new Random().nextInt(1000000000)) + ANDROID;
                if (!list.contains(id)) break;
            }
            list.add(id);
            return id;
        }
    }
}