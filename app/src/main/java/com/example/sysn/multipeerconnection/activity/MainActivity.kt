package com.example.sysn.multipeerconnection.activity

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.activity.home.HomeActivity
import com.example.sysn.multipeerconnection.connection.Connect
import com.example.sysn.multipeerconnection.connection.Manager
import com.example.sysn.multipeerconnection.etc.A.*
import com.example.sysn.multipeerconnection.etc.Util
import com.example.sysn.multipeerconnection.money.HelperWallet
import com.example.sysn.multipeerconnection.money.Wallet
import com.google.firebase.analytics.FirebaseAnalytics
import shinyoshiaki.blockchain.blockchain.BlockChain
import shinyoshiaki.blockchain.blockchain.PBFT
import shinyoshiaki.blockchain.etc.AESEncrypt
import shinyoshiaki.blockchain.etc.F
import shinyoshiaki.blockchain.etc.F.*
import shinyoshiaki.blockchain.webrtc.WebRTC
import java.util.*

class MainActivity : AppCompatActivity(), F.GetNTP.Listener, Manager.Listener {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    var progressDialog: ProgressDialog? = null
    var done = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 1)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)

        WebRTC.initWebRTC(this)

        getNTP(this)

        mainActivity = this
        ANDROID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        blockChain = BlockChain()
        pbft = PBFT()
        aesEncrypt = AESEncrypt()
        wallet = Wallet()
        helperWallet= HelperWallet()
        util = Util()

        log("key test")
        log(AESEncrypt().decy(AESEncrypt().ency("test", "pass"), "pass"))

        progressDialog = util.progress("ピアに接続中", this)

        if (save) {
            BlockChain().loadBlockchain(this)
        }
    }

    override fun onNTP(_date: Date?) {
        log("get ntp!! -> " + _date)
        TIME_LOGIN = _date

        runOnUiThread {
            connect = Connect()
            manager = Manager(connect, pbft, this)
        }
    }

    override fun onManager(value: String) {
        if (done) return
        done = true
        progressDialog!!.dismiss()
        startActivity(Intent(applicationContext, HomeActivity::class.java))
    }
}
