package com.example.sysn.multipeerconnection.etc

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Point
import android.support.v7.app.AlertDialog
import android.util.AndroidRuntimeException
import android.view.WindowManager
import android.widget.ImageView
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import shinyoshiaki.blockchain.etc.F
import shinyoshiaki.blockchain.etc.F.toast

/**
 * Created by shiny on 2017/11/15.
 */
class Util {
    fun msgWindow(activity: Activity, title: String, msg: String) {
        activity.runOnUiThread {
            AlertDialog.Builder(activity)
                    .setTitle(title)
                    .setMessage(msg)
                    .setPositiveButton("close") { _, _ -> }
                    .show()
        }
    }

    fun display(string: String, image: ImageView, activity: Activity) {
        if (string.length < 2953)
            try {
                val wm = activity.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                val disp = wm.defaultDisplay
                val xy = Point()
                disp.getSize(xy)
                val size = xy.x / 10 * 8
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.encodeBitmap(string, BarcodeFormat.QR_CODE, size, size)

                image.setImageBitmap(bitmap)

            } catch (e: WriterException) {
                throw AndroidRuntimeException("Barcode Error.", e)
            }
        else {
            toast(activity, "文字列が長すぎます")
        }
    }

    fun progress(msg: String, activity: Activity): ProgressDialog {
        val progressDialog = ProgressDialog(activity)
        progressDialog.isIndeterminate = true
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.setMessage(msg)
        progressDialog.show()
        return progressDialog
    }
}