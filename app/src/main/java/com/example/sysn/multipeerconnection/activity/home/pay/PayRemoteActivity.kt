package com.example.sysn.multipeerconnection.activity.home.pay

/**
 * Created by yoshiaki on 2017/07/20.
 */

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.activity.ReadQrActivity
import com.example.sysn.multipeerconnection.etc.A.*
import kotlinx.android.synthetic.main.activity_pay_remote.*
import org.json.JSONArray
import org.json.JSONObject

class PayRemoteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay_remote)

        text_payremote.setTextIsSelectable(true)

        wallet.refresh()

        btn_payremote.setOnClickListener {
            val target = edit_payremote_target.text.toString()
            val num = edit_payremote_num.text.toString().toInt()

            if (num > 0) {
                val json = wallet.pay.remotePay(target, num)     //paying
                val arr = json!!.getJSONArray("coins")
                val tranId = json.getString("tran_id")

                if (arr != null) {
                    val dialog = util.progress("決済処理中", this)

                    Handler().postDelayed({
                        dialog.dismiss()

                        if (wallet.isTransactionSuccess()) {     //check newest transactionId
                            text_payremote.text = arr.toString()

                            helperWallet.myTransactions.add(tranId, arr.toString())

                            util.display(arr.toString(), image_payremote, this)
                            util.msgWindow(this, "支払い完了", "この文字列(QRコード)を相手に届けてください")
                        } else {
                            util.msgWindow(this, "エラー", "決済失敗")
                            Handler().postDelayed({ finish() }, 3000)
                        }
                    }, 3500)

                } else {
                    util.msgWindow(this, "金額エラー", "残高不足")
                    Handler().postDelayed({ finish() }, (4000).toLong())
                }
            }
        }

        btn_payremote_return.setOnClickListener {
            finish()
        }

        btn_payremote_visibletxt.setOnClickListener {
            if (text_payremote.visibility == View.GONE)
                text_payremote.visibility = View.VISIBLE
            else
                text_payremote.visibility = View.GONE
        }

        btn_payremote_qr.setOnClickListener {
            startActivity(Intent(applicationContext, ReadQrActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        edit_payremote_target.setText(ReadQrActivity.result)
    }

}