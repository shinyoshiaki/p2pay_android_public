package shinyoshiaki.blockchain.etc;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventListener;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by yoshiaki on 2017/08/11.
 */

public class F {

    public static void setText(String string, TextView textView) {
        textView.setText(textView.getText() + "\n" + string);
    }

    public static String sha256(String plaintext) {
        String value = null;
        String algo = "HmacSHA256";
        try {
            String secret = "ShinYoshiaki";
            SecretKeySpec sk = new SecretKeySpec(secret.getBytes(), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(sk);

            byte[] mac_bytes = mac.doFinal(plaintext.getBytes());

            StringBuilder sb = new StringBuilder(2 * mac_bytes.length);
            for (byte b : mac_bytes) {
                sb.append(String.format("%02x", b & 0xff));
            }
            value = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String encodeBase64(String str) {
        String encoded = Base64.encodeToString(str.getBytes(), Base64.URL_SAFE);
        log("base64 encode:" + encoded);
        log(str.length() + "->" + encoded.length());
        return encoded;
    }

    public static String decodeBase64(String encoded) {
        log("base64 decode:" + new String(Base64.decode(encoded.getBytes(), Base64.URL_SAFE)));
        return new String(Base64.decode(encoded.getBytes(), Base64.URL_SAFE));
    }

    public static void toast(final Activity activity, final String st) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, st, Toast.LENGTH_SHORT).show();
                System.out.println("toast:" + st);
            }
        });
    }

    public static void log(String string) {
        System.out.println("log:" + string);
    }

    public static void saveJSON(String address, JSONObject jsonObject, Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences("database", MODE_PRIVATE);
        preferences.edit().putString(address, jsonObject.toString()).apply();
    }

    public static void saveJSONArray(String address, JSONArray jsonArray, Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences("database", MODE_PRIVATE);
        preferences.edit().putString(address, jsonArray.toString()).apply();
    }

    public static JSONObject loadJSON(String address, Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences("database", MODE_PRIVATE);
        try {
            return new JSONObject(preferences.getString(address, "null"));
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONArray loadJSONArray(String address, Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences("database", MODE_PRIVATE);
        try {
            return new JSONArray(preferences.getString(address, "null"));
        } catch (JSONException e) {
            return null;
        }
    }

    public static void saveString(String address, String value, Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences("database", MODE_PRIVATE);
        preferences.edit().putString(address, value).apply();
    }

    public static String loadString(String address, Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences("database", MODE_PRIVATE);
        return preferences.getString(address, "null");
    }

    public static void getNTP(GetNTP.Listener _listener) {
        new GetNTP(_listener).execute();
    }

    public static class GetNTP extends AsyncTask<Integer, Integer, Integer> {
        Listener listener;

        GetNTP(Listener _listener) {
            listener = _listener;
        }

        @Override
        protected Integer doInBackground(Integer... value) {
            try {
                NTPUDPClient ntpClient = new NTPUDPClient();
                ntpClient.open();
                String ntpServer;
                ntpServer = "ntp.nict.jp";
                TimeInfo timeInfo = null;
                try {
                    timeInfo = ntpClient.getTime(InetAddress.getByName(ntpServer));
                } catch (java.io.IOException ignored) {
                }
                ntpClient.close();
                assert timeInfo != null;
                log("NTP " + String.format("%-24s", ntpServer) + ": " + new Date(timeInfo.getReturnTime()));
                listener.onNTP(new Date(timeInfo.getReturnTime()));
            } catch (java.net.SocketException ignored) {
            }
            return 0;
        }

        public interface Listener extends EventListener {
            void onNTP(final Date _date);
        }
    }

    public static String date2string(Date _date) {
        String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
        String value = new SimpleDateFormat(DATE_PATTERN).format(_date);
        return value;
    }

    public static Date string2date(String value) {
        String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
        Date date = null;
        try {
            date = new SimpleDateFormat(DATE_PATTERN).parse(value);
        } catch (java.text.ParseException ignored) {
        }
        return date;
    }
}
