package com.example.sysn.multipeerconnection.activity.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.activity.AdminActivity
import com.example.sysn.multipeerconnection.activity.ShowQrActivity
import com.example.sysn.multipeerconnection.activity.TestBlockchainActivity
import com.example.sysn.multipeerconnection.activity.home.pay.PayLocalActivity
import com.example.sysn.multipeerconnection.activity.home.pay.PayRemoteActivity
import com.example.sysn.multipeerconnection.activity.home.pay.ReceiveRemoteActivity
import com.example.sysn.multipeerconnection.connection.Manager
import com.example.sysn.multipeerconnection.etc.A.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home_history.view.*
import kotlinx.android.synthetic.main.activity_home_menu.view.*
import org.json.JSONException
import org.json.JSONObject
import shinyoshiaki.blockchain.blockchain.Block
import shinyoshiaki.blockchain.blockchain.BlockChain
import shinyoshiaki.blockchain.blockchain.BlockChain.Companion.blockchain

/**
 * Created by shiny on 2017/09/20.
 */

class HomeActivity : AppCompatActivity(), Manager.Listener, ViewPager.OnPageChangeListener {
    var blockChain: BlockChain = BlockChain()
    var totalMoney = -1
    val PAGENUM = 2
    var activity: Activity = this

    companion object {
        var history: History? = null
    }

    override fun onStart() {
        super.onStart()
        if (ANDROID == null) finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        manager?.listener = this

        text_home_aid.text = ANDROID

        btn_home_test.setOnClickListener { startActivity(Intent(applicationContext, TestBlockchainActivity::class.java)) }
        btn_home_admin.setOnClickListener { startActivity(Intent(applicationContext, AdminActivity::class.java)) }
        btn_home_detail.setOnClickListener { startActivity(Intent(applicationContext, DetailActivity::class.java)) }

        val adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getItem(position: Int): Fragment = TestFragment.newInstance(position + 1)

            override fun getPageTitle(position: Int): CharSequence {
                when (position + 1) {
                    1 -> return "メニュー"
                    2 -> return "タスク"
                }
                return "error"
            }

            override fun getCount(): Int = PAGENUM
        }


        pager_home.adapter = adapter
        pager_home.addOnPageChangeListener(this)
        tabs_home.setupWithViewPager(pager_home)

        val handler = Handler()
        Handler().post(object : Runnable {
            override fun run() {
                if (blockchain.size > 0 && wallet != null)
                    text_home_money.text = wallet.myCoins.total().toString() + " yen"
                handler.postDelayed(this, 1000)
            }
        })

        btn_home_showqr.setOnClickListener {
            ShowQrActivity.show = ANDROID
            startActivity(Intent(applicationContext, ShowQrActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        manager?.listener = this

        blockChain.saveBlockchain(this)

        Handler().postDelayed({ wallet.refresh() }, 1000)

        runOnUiThread {
            Handler().postDelayed({
                if (totalMoney != wallet.myCoins.total()) {
                    if (totalMoney != -1) {
                        val title = if (totalMoney < wallet.myCoins.total())
                            "受け取りました"
                        else
                            "支払いました"
                        AlertDialog.Builder(this)
                                .setTitle(title)
                                .setMessage(Math.abs(totalMoney - wallet.myCoins.total()).toString())
                                .setPositiveButton("ok") { _, _ -> }
                                .show()
                    }
                    totalMoney = wallet.myCoins.total()
                }
            }, 2000)
        }

        if (save) {
            blockChain.saveBlockchain(this)
        }
    }

    override fun onManager(value: String) {
        runOnUiThread { Handler().postDelayed({ if (wallet != null) wallet.refresh() }, (1000).toLong()) }
        JSONObject(value).let {

        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        when (position + 1) {
            1 -> {
            }
            2 -> {
                history?.refresh()
            }
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}

    class TestFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            var view: View? = null
            when (arguments.getInt("page", 0)) {
                1 -> {
                    view = inflater!!.inflate(R.layout.activity_home_menu, container, false)
                    Menu(activity, view)
                }
                2 -> {
                    view = inflater!!.inflate(R.layout.activity_home_history, container, false)
                    history = History(activity, view)
                    history?.refresh()
                }
            }
            return view
        }

        companion object {
            fun newInstance(page: Int): TestFragment {
                val args = Bundle()
                args.putInt("page", page)
                val fragment = TestFragment()
                fragment.arguments = args
                return fragment
            }
        }
    }

    class Menu(val activity: Activity, val view: View) {
        init {
            view.btn_home_remotepay.setOnClickListener { activity.startActivity(Intent(activity, PayRemoteActivity::class.java)) }
            view.btn_home_localpay.setOnClickListener { activity.startActivity(Intent(activity, PayLocalActivity::class.java)) }
            view.btn_home_charge.setOnClickListener { activity.startActivity(Intent(activity, ChargeActivity::class.java)) }
        }
    }

    class History(val activity: Activity, val view: View) {
        fun refresh() {
            val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1)

            val clone = blockchain.clone() as ArrayList<Block>
            for (block in clone) {
                try {
                    val json = JSONObject(block.data)
                    val order = json.getString(ORDER)

                    when (order) {
                        "pay" -> {
                            val tranId = json.getString("tran_id")
                            if (!helperWallet.myTranIds.v.contains(tranId)) {
                                if (json.getString("receive") == ANDROID) {
                                    adapter.add(tranId + "," + "receive")
                                } else if (json.getString("pay") == ANDROID) {
                                    adapter.add(tranId + "," + "pay")
                                }
                            }
                        }
                    }
                } catch (e: JSONException) {
                }
            }

            view.list_history.adapter = adapter
            view.list_history.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
                val list = parent as ListView
                val item = list.getItemAtPosition(position) as String

                when (item.split(",")[1]) {
                    "receive" -> {
                        ReceiveRemoteActivity.tranId = item.split(",")[0]
                        activity.startActivity(Intent(activity, ReceiveRemoteActivity::class.java))
                    }
                    "pay" -> {
                        ShowQrActivity.show = helperWallet.myTransactions.getTransaction(item.split(",")[0])!!
                        activity.startActivity(Intent(activity, ShowQrActivity::class.java))
                    }
                }
            }
        }
    }
}
