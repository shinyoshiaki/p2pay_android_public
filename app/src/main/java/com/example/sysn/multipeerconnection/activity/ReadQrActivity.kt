package com.example.sysn.multipeerconnection.activity

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import kotlinx.android.synthetic.main.activity_readqr.*
import kotlinx.android.synthetic.main.activity_recieve_remote.*
import org.json.JSONException

/**
 * Created by shiny on 2017/12/07.
 */
class ReadQrActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_readqr)

        barcode_readqr.decodeSingle(object : BarcodeCallback {
            override fun barcodeResult(barcodeResult: BarcodeResult) {
                result = barcodeResult.text.toString()

                runOnUiThread { Handler().postDelayed({ finish() }, (1000).toLong()) }

                barcode_readqr.pause()
            }

            override fun possibleResultPoints(list: List<ResultPoint>) {}
        })
    }

    companion object {
        var result = ""
    }

    override fun onResume() {
        super.onResume()
        barcode_readqr.resume()
    }

    override fun onPause() {
        super.onPause()
        barcode_readqr.pause()
    }
}