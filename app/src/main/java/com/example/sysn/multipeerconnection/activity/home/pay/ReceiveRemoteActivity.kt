package com.example.sysn.multipeerconnection.activity.home.pay

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.etc.A.*
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import kotlinx.android.synthetic.main.activity_recieve_remote.*
import org.json.JSONArray
import org.json.JSONException
import shinyoshiaki.blockchain.etc.F.log

/**
 * Created by shiny on 2017/12/02.
 */

class ReceiveRemoteActivity : AppCompatActivity() {
    companion object {
        var tranId = "" //intent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recieve_remote)

        if (wallet != null) wallet.refresh()

        util.msgWindow(this, "決済", "コインを受け取りました。\n文字列を入力するかQRコードを読み込んでください")

        btn_receiveremote.setOnClickListener {
            JSONArray(edit_receiveremote.text.toString()).let {
                helperWallet.receiveRemote(it, tranId)
                Handler().postDelayed({ finish() }, (1000).toLong())
            }
        }

        barcode_receiveremote.decodeSingle(object : BarcodeCallback {
            override fun barcodeResult(barcodeResult: BarcodeResult) {
                val result = barcodeResult.text.toString()

                log("receive qr result:" + result)
                try {
                    JSONArray(result).let {
                        helperWallet.receiveRemote(it, tranId)

                        log("receive")
                        runOnUiThread { Handler().postDelayed({ finish() }, (1000).toLong()) }
                    }
                } catch (e: JSONException) {
                    log("receive remote json parse error barcode_receiveremote.decodeSingle")
                }

                barcode_receiveremote.pause()
            }

            override fun possibleResultPoints(list: List<ResultPoint>) {}
        })

        btn_recieveremote_return.setOnClickListener {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        barcode_receiveremote.resume()
    }

    override fun onPause() {
        super.onPause()
        barcode_receiveremote.pause()
    }
}