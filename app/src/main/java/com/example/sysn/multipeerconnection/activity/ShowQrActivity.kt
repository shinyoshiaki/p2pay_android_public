package com.example.sysn.multipeerconnection.activity

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.sysn.multipeerconnection.R
import com.example.sysn.multipeerconnection.etc.A.util
import kotlinx.android.synthetic.main.activity_showqr.*

/**
 * Created by shiny on 2017/12/07.
 */
class ShowQrActivity : AppCompatActivity() {
    companion object {
        var show = ""       //intent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_showqr)

        util.display(show, image_showqr, this)
        text_showqr.text = show

        btn_showqr_return.setOnClickListener {
            finish()
        }

        runOnUiThread { Handler().postDelayed({ finish() }, (1000 * 10).toLong()) }
    }
}